package com.empulse.wiltmann.rest;

import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.RCallerService;
import com.empulse.wiltmann.service.impl.DefaultDataService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * all plain unit-test methods for DataDaoService.
 * no springBoot context needed
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-16
 */
class DataDaoServiceTest {

    @Mock
    RCallerService rCallerService;

    @Mock
    FileService fileService;

    private final DefaultDataService DATA_DAO_SERVICE = new DefaultDataService(rCallerService, fileService);
    private final String FILE_CONTENT = "Test World!";
    private final String R_SCRIPT = "json.R";
    private final String JSON_STRING_VALUE = "[{\"value\":1}]";
    private final String ANY_REGEX_PATTERN = ".+";

    @Test
    void callrwrapper() {
        assertEquals(JSON_STRING_VALUE, DATA_DAO_SERVICE.callrwrapper(R_SCRIPT, JSON_STRING_VALUE).get());
    }

    @Test
    void saveJsonInputToFile() {
        assertEquals(true, DATA_DAO_SERVICE.saveJsonInputToFile(FILE_CONTENT));
    }

    @Test
    void getLatestSavedJson() {
        assertThat(DATA_DAO_SERVICE.getLatestSavedJson().toString()).matches(Pattern.compile(ANY_REGEX_PATTERN));
    }
}