package com.empulse.wiltmann.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * all plain unit-test methods for DataDaoServiceUtils
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-15
 */
class DataDaoServiceUtilsTest {

//    @Mock
//    RCallerService rCallerService;
//
//    private final String RECENT_DATE_REGEX_PATTERN = "20\\d{6}";
//
//    private final String FILE_CONTENT = "Test World!";
////    DataDaoService dataDaoService = new DataDaoService(rCallerService, fileService);
////    private final Path FILE_PATH = dataDaoService.getFILE_PATH();
////    private final String FILE_NAME = FILE_PATH.toString() + "/test.json";
////    private final Path WRITTEN_FILE_PATH = Paths.get(FILE_NAME);
//    private boolean fileWritten = false;
//
//    @Test
//    void recentDateFormatter() {
//        assertThat(DataDaoServiceUtils.recentDateFormatter()).matches(Pattern.compile(RECENT_DATE_REGEX_PATTERN));
//    }
//
//    @Test
//    void writeToFile() {
//        assertTrue(DataDaoServiceUtils.writeToFile(FILE_NAME, FILE_CONTENT));
//    }
//
//    @Test
//    void readStringFromFilePath() {
//        assertEquals(FILE_CONTENT, DataDaoServiceUtils.readStringFromFilePath(WRITTEN_FILE_PATH).get());
//    }
}