package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.JsonFormatterService;
import com.empulse.wiltmann.service.RCallerService;
import com.empulse.wiltmann.service.RCodeService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"rScript.basePath=./"})
public class DefaultRCallerServiceTest {

    private static final Logger log = LoggerFactory.getLogger(DefaultRCallerServiceTest.class);

    JsonFormatterService jsonFormatterService = new DefaultJsonFormatterService();
    @MockBean
    FileService fileService;
    RCodeService rCodeService = new DefaultRCodeService(fileService);
    RCallerService rCallerService = new DefaultRCallerService(jsonFormatterService, rCodeService);

    @Test
    public void processRscript() {
        String forecast = "";
        try {
            forecast = rCallerService.processRscript("json.R", "[{\"value\" : 1}]").get();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage());
        } finally {
            assertFalse(forecast.isBlank());
        }
        assertEquals("[{\"value\":1}]",forecast);

    }

    /**
     * This test checks if the arima file is processed correctly using the {@link RCallerService#processRscript(String, String)} method
     * in a multithreaded context by comparing the structure of the resulting json
     */
    @Test
    public void processArima() {
        ObjectMapper jsonMapper = new ObjectMapper();
        String jsonValues = null;
        List<Map<String, Object>> resultJsonObject = null;
        try {
            jsonValues = fileService.getFileContent("Export_8021121.json");
            resultJsonObject = jsonMapper.readValue(fileService.getFileContent("0_TEST_EX_FORECAST_RESULTS.JSON"), new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            assertNotNull(jsonValues);
            assertNotNull(resultJsonObject);
        }
        List<Map<String, Object>> forecast = null;
        try {
            forecast = jsonMapper.readValue(rCallerService.processRscript("simple_arima-javalight.R", jsonValues).get(), new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (IOException | InterruptedException | ExecutionException e) {
            log.error(e.getMessage());
        } finally {
            assertNotNull(forecast);
        }
        for (int i = 0; i < forecast.size(); i++) {
            assertEquals(resultJsonObject.get(0).keySet(), forecast.get(0).keySet());
        }
    }

    @Test
    public void processArimaByLine() {
        String jsonValues = null;
        try {
            jsonValues = fileService.getFileContent("Export_8021121.json");

        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            assertNotNull(jsonValues);
        }
        String forecast = null;
        try {
            forecast = rCallerService.processRscript("simple_arima-javalight.R", jsonValues).get();
            System.out.println("forecast: " + forecast);
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage());
        } finally {
            assertNotNull(forecast);
        }
    }
}