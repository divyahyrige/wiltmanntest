package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.JsonFormatterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
@RunWith(SpringRunner.class)
public class DefaultJsonFormatterServiceTest {

    private static final Logger log = LoggerFactory.getLogger(DefaultJsonFormatterServiceTest.class);

    @TestConfiguration
    static class DefaultJsonFormatterServiceTestConfiguration {

        @Bean
        public JsonFormatterService jsonFormatterService() {
            return new DefaultJsonFormatterService();
        }
    }

    @Autowired
    private JsonFormatterService jsonFormatterService;

    @Test
    public void stripJsonInputTest() {
        String beautifiedJson = jsonFormatterService.minify(getBeautifiedJson());
        assertEquals(getUnbeautifiedJson(), beautifiedJson);
    }
    @Test(expected = IllegalArgumentException.class)
    public void stripJsonInputTestWithNullParam() {
        String beautifiedJson = jsonFormatterService.minify(null);
    }

    private String getUnbeautifiedJson() {
        return "[{\"somekey\":\"some value with \\\"withespaces\\\"\",\"someOtherKey\":\"someOtherValueWithoutWithespaces\"}]";
    }

    private String getBeautifiedJson() {
        return "\n\n[\n  {\n   \"somekey\" : \"some value with \\\"withespaces\\\"\",\n    \"someOtherKey\" : \"someOtherValueWithoutWithespaces\"\n}\n]\n\n\n";
    }
}