package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.RCodeService;
import com.github.rcaller.rstuff.RCode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-07-09
 */
public class DefaultRCodeServiceTest {

    private static final Logger log = LoggerFactory.getLogger(DefaultRCodeServiceTest.class);
    private final List<String> code;

    @MockBean
    FileService fileService = Mockito.mock(FileService.class);

    RCodeService rCodeService = new DefaultRCodeService(fileService);

    public DefaultRCodeServiceTest() {
        this.code = new ArrayList<String>(
                Arrays.asList(
                        "jsonprocess <- function(jsondata) {",
                        "    # This is a hello-world script for testing purposes.",
                        "    print(\"Hello World!\")",
                        "}"
                )
        );
    }

    @Before
    public void setUp() {
        try {
        Mockito.when(fileService.getFileContentByLine(anyString())).thenReturn(code);
        } catch (IOException e) {
            log.error("{}", e);
        }
    }

    @Test
    public void getRCodeTest() {
        String expectedResult = "";
        String actualResult = "";
        try {
            RCode expectedRCode = RCode.create();
            for (String line: code) {
                expectedRCode.addRCode(line);
            }
            expectedResult = expectedRCode.getCode().toString();
            actualResult = rCodeService.getRCode("code.R").toString();
        } catch (IOException e) {
            log.error("{}", e);
        } finally {
            Assert.assertFalse(expectedResult.isBlank());
            Assert.assertFalse(actualResult.isBlank());
        }
        assertEquals(expectedResult, actualResult);
    }
}