package com.empulse.wiltmann.statics;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-07-10
 */
public final class ApplicationStatics {
    public static final String R_SCRIPT_CACHE_NAME = "rScriptCache";
}
