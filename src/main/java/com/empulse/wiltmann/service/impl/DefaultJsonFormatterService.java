package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.JsonFormatterService;
import org.springframework.stereotype.Service;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
@Service
public final class DefaultJsonFormatterService implements JsonFormatterService {
    @Override
    public String minify(String jsonvalue) {
        if (null == jsonvalue) throw new IllegalArgumentException();
        return jsonvalue.strip().replace("\n", "")
                .replace("\r", "")
                .replace(" : ", ":")
                .replace("  ", "")
                .replace(" \"", "\"")
                .replace("\" ", "\"")
                .replace(", ", ",")
                .replace("\' ", "\'")
                .replace(" \'", "\'");
    }
}
