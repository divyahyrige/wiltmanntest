package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.FileService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
@Service
public final class DefaultFileService implements FileService {

    private static final Logger log = LoggerFactory.getLogger(DefaultFileService.class);

    private final ResourceLoader resourceLoader;
    private final Path fileSaveLocationPath;

    @Value("${rscript.basePath}")
    private String rscriptBasePath;

    @Autowired
    public DefaultFileService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
        this.fileSaveLocationPath = Paths.get("files");
        try {
            Files.createDirectories(this.fileSaveLocationPath);
        } catch (IOException e) {
            log.error("{}", e);
        }
    }

    @Override
    public String getFileContent(String filename) throws IOException {
        Resource rScriptUri = resourceLoader.getResource("file:"+rscriptBasePath+filename);
        return FileUtils.readFileToString(rScriptUri.getFile(), Charset.forName("utf-8"));
    }

    @Override
    public List<String> getFileContentByLine(String filename) throws IOException {
        Resource rScriptUri = resourceLoader.getResource("file:"+rscriptBasePath+filename);
        return FileUtils.readLines(rScriptUri.getFile(), Charset.forName("UTF-8"));
    }

    @Override
    public boolean isScriptExistent(String filename){
        Resource resource = resourceLoader.getResource("file:"+rscriptBasePath+filename);
        return resource.exists();
    }

    @Override
    public Optional<File> save(String filename, String content) {
        try {
            if (null == filename || null == content) throw new IllegalArgumentException();
            return Optional.of(Files.write(Paths.get(fileSaveLocationPath.resolve(filename).toString()), content.getBytes()).toFile());
        } catch (IOException | IllegalArgumentException e) {
            log.error("{}", e);
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(File file) {
        try {
            Files.delete(file.toPath());
            return true;
        } catch (IOException e) {
            log.error("{}", e);
            return false;
        }
    }

    @Override
    public Optional<File> get(String filename) {
        if (null == filename) throw new IllegalArgumentException();
        return Optional.of(Paths.get(fileSaveLocationPath.resolve(filename).toString()).toFile());
    }

    @Override
    public Optional<File> getLatestSavedFile() {
        File folder = new File(fileSaveLocationPath.toString());
        return Stream.of(folder.listFiles()).sorted((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName())*(-1))
                .findFirst();
    }
}
