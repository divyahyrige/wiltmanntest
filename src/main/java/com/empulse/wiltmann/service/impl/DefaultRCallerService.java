package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.JsonFormatterService;
import com.empulse.wiltmann.service.RCallerService;
import com.empulse.wiltmann.service.RCodeService;
import com.github.rcaller.rstuff.*;
import com.github.rcaller.util.Globals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.concurrent.Future;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
@Service
@Async("rThreadPoolTaskExecutor")
public class DefaultRCallerService implements RCallerService {

    private static final Logger log = LoggerFactory.getLogger(DefaultRCallerService.class);
    private static final String R_INPUT_VARIABLE_NAME = "input";
    private static final String R_OUTPUT_VARIABLE_NAME = "output";
    private static final String R_RETURN_VARIABLE_NAME = "returnable";
    private static final String WRITE_R_FUNCTION_RESULT_INTO_RESULT = R_RETURN_VARIABLE_NAME + " <- getResult(" + R_OUTPUT_VARIABLE_NAME + ");";

    private final JsonFormatterService jsonFormatterService;
    private final RCodeService rCodeService;

    @Autowired
    public DefaultRCallerService(JsonFormatterService jsonFormatterService, RCodeService rCodeService) {
        this.jsonFormatterService = jsonFormatterService;
        this.rCodeService = rCodeService;
    }
    @Override
    public Future<String> processRscript(String filename, String jsonvalues) {
        try {
            long timeStart = System.currentTimeMillis();
            RCode code = RCode.create();
            jsonvalues = makeJsonvaluesRcallerUsable(jsonvalues);
            code.addString(R_INPUT_VARIABLE_NAME, jsonvalues);
            code.addRCode(getResultRFunction());
            code.addRCode(rCodeService.getRCode(filename));
            code.addRCode(WRITE_R_FUNCTION_RESULT_INTO_RESULT);
            RCaller caller = RCaller.create(code, RCallerOptions.create());
            caller.runAndReturnResult(R_RETURN_VARIABLE_NAME);
            long processingTime = System.currentTimeMillis()-timeStart;
            log.debug("R execution time: {} ms", processingTime);
            String result = caller.getParser().getAsStringArray(R_RETURN_VARIABLE_NAME)[0];
            return new AsyncResult<>(null == result ? "" : result);
        } catch (IOException e) {
            log.error("Error in script {} : [{}]", filename, e);
            return new AsyncResult<>("");
        }
    }



    @Override
    public Future<String> processRscriptFileInput(String filename, File jsonFile) {
        String returnVar = "";
        try {
            long timeStart = System.currentTimeMillis();
            RCode code = RCode.create();
            code.addRCode(getResultRFunction());
            code.addRCode(setWorkingDirectoryRFunction(jsonFile.getAbsolutePath()));
            code.addString(R_INPUT_VARIABLE_NAME, jsonFile.getName());
            code.addRCode(rCodeService.getRCode(filename));
            code.addRCode(WRITE_R_FUNCTION_RESULT_INTO_RESULT);
            log.debug("{}", code);
//            RCallerOptions options = RCallerOptions.create(Globals.RScript_Linux, Globals.R_Linux, FailurePolicy.RETRY_1, 3000l, 100l, RProcessStartUpOptions.create());
//            RCallerOptions options = RCallerOptions.create(Globals.Rscript_current, Globals.R_current, FailurePolicy.RETRY_5, 9223372036854775807L, 100L, RProcessStartUpOptions.create());
//            RCaller caller = RCaller.create(code, options);


            RCaller caller = RCaller.create(code, RCallerOptions.create());
            caller.runAndReturnResult("result");
            caller.runAndReturnResult(R_RETURN_VARIABLE_NAME);
            long processingTime = System.currentTimeMillis() - timeStart;
            log.info("R execution time: {} ms", processingTime);
            returnVar = caller.getParser().getAsStringArray(R_RETURN_VARIABLE_NAME)[0];
        } catch (IOException e) {
            log.error("Error in script {} : [{}]", filename, e);
        }
        return new AsyncResult<>(null == returnVar ? "" : returnVar);
    }



    private String makeJsonvaluesRcallerUsable(String jsonvalues) {
        return jsonFormatterService.minify(jsonvalues).replaceAll("\"","'");
    }

    private String getResultRFunction() {
        return"getResult <- function(param) {\n" +
                "\tsprintf(\"%s\",toJSON(param));\n" +
                "}";
    }

    private String setWorkingDirectoryRFunction(String absoluteFilePath) {
        String separator = FileSystems.getDefault().getSeparator();
        if (separator.equals("\\")) {
            separator = "\\\\";
        }
        String[] directories = absoluteFilePath.split(separator);
        StringBuffer setwd = new StringBuffer("setwd(file.path(");
        for (int i=0; i<directories.length-1; i++) {
            setwd.append("\"").append(directories[i]).append("\"");
            if (i<directories.length-2) setwd.append(",");
        }
        return setwd.append("));").toString();
    }
}
