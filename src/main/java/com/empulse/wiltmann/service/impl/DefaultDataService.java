package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.DataService;
import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.RCallerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Stream;

/**
 * handling the REST endpoints' functionality from DataController
 * FILES ARE WRITTEN & READ FROM: resources/files
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-08
 */
@Service
public class DefaultDataService implements DataService {

    private static final String INPUT_JSON_ENDING = "input.json";

    private final Logger logger = LoggerFactory.getLogger(DefaultDataService.class);
    private final RCallerService rCallerService;
    private final FileService fileService;

    /**
     * @param rCallerService the service class handling the script methods used
     * @param fileService the service class handling the file methods used
     */
    @Autowired
    public DefaultDataService(RCallerService rCallerService, FileService fileService) {
        this.rCallerService = rCallerService;
        this.fileService = fileService;
    }

    /**
     * @param rScript the r-script used by the r-wrapper taken from application properties
     * @param rawJson the Json-structure that is passed into our service to be processed by the r-script
     * @return the Json-structure that comes as result from the r-script
     */
    @Override
    public Optional<String> callrwrapper(String rScript, String rawJson) {
        try {
            Future<String> future = rCallerService.processRscript(rScript, rawJson);
            return Optional.ofNullable(future.get());

        } catch (ExecutionException | InterruptedException e) {
            logger.error("{}", e);
        }
        return Optional.empty();
    }

    /**
     * @param rScript the r-script used by the r-wrapper taken from application properties
     * @param fileJson the Json-structure that is passed into our service to be processed by the r-script
     * @return the Json-structure that comes as result from the r-script
     */
    @Override
    public Optional<String> callrwrapper(String rScript, File fileJson) {
        try {
            Future<String> future = rCallerService.processRscriptFileInput(rScript, fileJson);
            return Optional.ofNullable(future.get());

        } catch (ExecutionException | InterruptedException e) {
            logger.error("{}", e);
            return Optional.empty();
        } finally {
            fileService.delete(fileJson);
        }
    }

    /**
     * writing a String as a .json file with recent timestamp to the FILE_PATH
     * example for the filename: 20200615_input.json
     * if there is already one file with this timestamp, the following files will get increment numbers to the name
     * like: 20200615_input_1.json
     * the method is returning the content from the written file as a proof
     *
     * @param rawJson the Json-structure that is passed into our service
     * @return String read from the file that was written by the method itself
     * @TODO log exception
     */
    @Override
    public Optional<File> saveJsonInputToFile(String rawJson) {
        String formattedDate = this.recentDateFormatter();
        String filename = formattedDate + "_" + System.currentTimeMillis() + "_" + INPUT_JSON_ENDING;
        return fileService.save(filename, rawJson);
    }

    /**
     * searches for the latest saved Json file in the directory: FILE_PATH
     * and returns the content as a String
     *
     * @return String with the content of the latest saved file
     * or returns errorMessage if unexpected error occurs
     * @TODO log exception
     */
    @Override
    public Optional<String> getLatestSavedJson() {
        Optional<File> latestFile = fileService.getLatestSavedFile();
        return latestFile.map(file -> readLineByLine(file.getPath()));
    }

    private static String readLineByLine(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    /**
     * returns now() in the expected format.
     *
     * @return the current timestamp formated like: yyyyMMdd
     */
    private String recentDateFormatter() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        return formatter.format(now);
    }
}