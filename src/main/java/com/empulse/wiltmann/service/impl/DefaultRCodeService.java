package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.RCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-07-08
 */
@Service
public final class DefaultRCodeService implements RCodeService {

    private final FileService fileService;

    @Autowired
    public DefaultRCodeService(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public String getRCode(String filename) throws IOException {
        List<String> fileLines = fileService.getFileContentByLine(filename);
        return String.join("\n", fileLines);
    }
}
