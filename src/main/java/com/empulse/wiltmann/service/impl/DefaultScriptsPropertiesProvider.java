package com.empulse.wiltmann.service.impl;

import com.empulse.wiltmann.service.ScriptsPropertiesProvider;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * provides the paths to the r-scripts from the properties
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-07-13
 */
@Service
public class DefaultScriptsPropertiesProvider implements ScriptsPropertiesProvider {

    private final Properties properties;

    /**
     * properties taken from script properties
     */
    public DefaultScriptsPropertiesProvider() {
        String propertyLocation = System.getProperty("scripts.config.location");
        System.out.println("Script property location is :"+ propertyLocation );
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(propertyLocation));
            this.properties = properties;
        } catch (IOException e) {
            throw new IllegalStateException("enable properties from " + propertyLocation, e);
        }
    }

    @Override
    public List<String> getKeys() {
        return Stream.of(properties.keys()).map(String.class::cast).collect(Collectors.toList());
    }

    @Override
    public String getValue(String key) {
        return properties.getProperty(key);
    }
}