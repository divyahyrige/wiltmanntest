package com.empulse.wiltmann.service;

import com.empulse.wiltmann.statics.ApplicationStatics;
import com.github.rcaller.rstuff.RCode;
import org.springframework.cache.annotation.Cacheable;

import java.io.IOException;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-07-08
 */
public interface RCodeService {

    /**
     * Generates an RCode Object with the RCode of a given R-Script.
     *
     * @param filename              Name of the R-Script that should be used, this name should include
     *                              the relative path to the file from the R-Script-Base-Directory
     *                              like {@code somedir/somefile.R}
     * @return                      {@link RCode} Object with the content of the given R-Script
     * @throws IOException
     */
    @Cacheable(value = ApplicationStatics.R_SCRIPT_CACHE_NAME, key = "#filename", sync = true)
    String getRCode(String filename) throws IOException;
}
