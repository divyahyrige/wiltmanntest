package com.empulse.wiltmann.service;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
public interface JsonFormatterService {
    /**
     * Strips the jsonvalue of all unnecessary linebreaks and whitespaces.
     *
     * @param jsonvalue A beautified JSON-String, should not be null
     * @return Minified JSON-String, will return an empty String if given null.
     * @throws IllegalArgumentException If the given parameter is {@code null}
     */
    String minify(String jsonvalue);
}
