package com.empulse.wiltmann.service;

import java.util.List;

/**
 * TODO: Add class description
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-07-13
 */
public interface ScriptsPropertiesProvider {

    /**
     *
     * @return a list of all keys from .properties
     */
    List<String> getKeys();

    /**
     * @param key the key in .properties to be asked for
     * @return the value of the given key
     */
    String getValue(String key);

}
