package com.empulse.wiltmann.service;

import java.io.File;
import java.util.Optional;

/**
 * TODO: Add class description
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-07-03
 */
public interface DataService {
    /**
     * @param rScript the R-script to be used
     * @param rawJson the data to be processed
     * @return the result of the R-script
     */
    Optional<String> callrwrapper(String rScript, String rawJson);

    /**
     * @param rScript the R-script to be used
     * @param fileJson the data to be processed
     * @return the result of the R-script
     */
    Optional<String> callrwrapper(String rScript, File fileJson);

    /**
     * @param rawJson the Json-structure to be saved
     * @return boolean true if file saved
     */
    Optional<File> saveJsonInputToFile(String rawJson);

    Optional<String> getLatestSavedJson();

}
