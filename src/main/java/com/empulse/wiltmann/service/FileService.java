package com.empulse.wiltmann.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
public interface FileService {
    /**
     * Loads an R script as textfile from resources folder.
     * @param filename
     * @return Filecontent as String
     * @throws IOException
     */
    String getFileContent(String filename) throws IOException;

    /**
     * Loads an R script as textfile from resources folder.
     * @param filename
     * @return Filecontent as String
     * @throws IOException
     */
    List<String> getFileContentByLine(String filename) throws IOException;

    boolean isScriptExistent(String filename);

    /**
     * Saves a file to the working directory.
     *
     * @param filename                          Name of the file to be saved.
     *                                          If the file is to be saved in a sub-folder
     *                                          of the working directory, the filename should
     *                                          contain the relative path from the working directory
     *                                          like : <b>{@code somedir/someotherdir/filename}</b>
     * @param content                           The content of the file to be saved
     * @return {@link Optional}<{@link File}>   If the file is saved successfully it will be then returned.
     */
    Optional<File> save(String filename, String content);

    /**
     * Deletes a File by the given File.
     *
     * @param file      The file that should be deleted.
     * @return boolean  Returns {@code TRUE} if the File is deleted successfully
     */
    boolean delete(File file);

    /**
     * Gets a file from the Working-Directory of the program.
     *
     * @param filename      Name of the file to be loaded.
     *                      The filename should contain the relative path from the Working-Directory
     *                      like : <b>{@code somedir/someotherdir/filename}</b>
     * @return {@link File} The File from the Working-Directory.
     */
    Optional<File> get(String filename);

    /**
     * Finds the latest saved file inside the Working-Directory
     * based on the file name.
     *
     * @return {@link Optional}<{@link File}>   Latest saved file inside the Working-Directory
     */
    Optional<File> getLatestSavedFile();
}
