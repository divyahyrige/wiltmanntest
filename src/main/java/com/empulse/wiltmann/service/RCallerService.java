package com.empulse.wiltmann.service;

import java.io.File;
import java.util.concurrent.Future;

/**
 * A Service that runs an R-Script
 * @author Julian Kupsch, empulse GmbH, 2020-06-30
 */
public interface RCallerService {
    /**
     * This method Processes an R-Script with a given filename.
     * It forwards the JSON-Values by setting a variable inside the R-Context holding the
     * {@param jsonvalues} as it's value.
     * the variable {@code input} which holds the filename containing the dataset
     * and the variable {@code output} which should hold the result of the R-Script
     * inside the R-Context.
     * This method should be replaced by {@link #processRscriptFileInput(String, File)} since it
     * makes sure, that even large datasets will be processed correctly.
     * @param filename                  Name of the R-Script to be executed.
     *                                  <b color="red">The filename should include the relative
     *                                  path from the {@code rScript.basePath} to the file
     *                                  like: {@code somedir/someotherdir/script.R}</b>
     * @param jsonvalues                The dataset to be used inside the R-Script.
     * @return {@link Future<String>}   Returns an asynchronous result containing a {@link String}
     *                                  holding the result of the R-Script.
     */
    Future<String> processRscript(String filename, String jsonvalues);

    /**
     * This method Processes an R-Script with a given filename.
     * It forwards the dataset using a File that should exist prior to
     * calling this method. The scripts run by this method can use
     * the variable <b>{@code input}</b> which holds the filename containing the dataset
     * and the variable <b>{@code output}</b> which should hold the result of the R-Script
     * inside the R-Context.
     * To ensure that the file contained in <b>{@code input}</b> can be process correctly
     * the working directory will be set inside the R-Context.
     * Through that it ensures that even large datasets can be used in the R-Context.
     * @param rScript                   Name of the R-Script to be executed.
     *                                  <b color="red">The filename should include the relative
     *                                  path from the {@code rScript.basePath} to the file
     *                                  like: {@code somedir/someotherdir/script.R}</b>
     * @param fileJson                  The dataset to be used inside the R-Script.
     *                                  This file should exist prior to executing this method.
     *                                  If not, the R-Script may not finish successfully.
     * @return {@link Future}<{@link String}>   Returns an asynchronous result containing a {@link String}
     *                                  holding the result of the R-Script.
     */
    Future<String> processRscriptFileInput(String rScript, File fileJson);


}
