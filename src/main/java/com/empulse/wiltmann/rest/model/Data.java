package com.empulse.wiltmann.rest.model;

/**
 * representing the Json structure of the in- and output of the webservice
 * NOT USED YET
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-08
 */
public class Data {

    private final String ID = "KW2020-01-8003510";
    private final String DATE = "2020-01-01";
    private final double DAYOFWEEK = 3;
    private final int AMOUNT = 0;
    private final String UNIT = "KG";
    private final String MATERIAL = "000000000008003510";
    private final String BOM = "000000000004002185";
    private final int WORKINGDAY = 0;
    private final int MONDAY = 0;
    private final int TUESDAY = 0;
    private final int WEDNESDAY = 1;
    private final int THURSDAY = 0;
    private final int FRIDAY = 0;
    private final int HOLIDAY = 1;
    private final int HOLIDAYWEEK = 1;
    private final int DAYSOFHOLIDAY = 1;
    private final int LONG1B = 0;
    private final int LONG2B = 0;
    private final int LONG1A = 0;
    private final int LONG2A = 0;
    private final int SHORT1B = 0;
    private final int SHORT1A = 0;
    private final int PROMO = 0;
    private final int PROMOVOLUME = 0;
    private final int PROMOBIG1B = 0;
    private final int PROMOBIG1A = 0;
    private final int PROMOBIG2A = 0;
    private final int PROMOBIG3A = 0;
    private final int PROMOSMALL1A = 0;
    private final int LISTING = 0;
    private final int DELISTING = 0;

    public Data(String ID, String DATE, int DAYOFWEEK, double AMOUNT, String UNIT, String MATERIAL, String BOM, int WORKINGDAY,
                int MONDAY, int TUESDAY, int WEDNESDAY, int THURSDAY, int FRIDAY, int HOLIDAY, int HOLIDAYWEEK, int DAYSOFHOLIDAY,
                int LONG1B, int LONG2B, int LONG1A, int LONG2A, int SHORT1B, int SHORT1A, int PROMO, int PROMOVOLUME, int PROMOBIG1B,
                int PROMOBIG1A, int PROMOBIG2A, int PROMOBIG3A, int PROMOSMALL1A, int LISTING, int DELISTING) {
    }

    public Data(String ID, String DATE, double DAYOFWEEK) {
    }

    public String getID() {
        return ID;
    }

    public String getDATE() {
        return DATE;
    }

    public double getAMOUNT() {
        return AMOUNT;
    }
}
