package com.empulse.wiltmann.rest;

import com.empulse.wiltmann.rest.exception.EmptyInputJsonException;
import com.empulse.wiltmann.rest.exception.EmptyOutputJsonException;
import com.empulse.wiltmann.rest.exception.NoScriptNamePassedException;
import com.empulse.wiltmann.rest.exception.NoSuchScriptException;
import com.empulse.wiltmann.service.DataService;
import com.empulse.wiltmann.service.FileService;
import com.empulse.wiltmann.service.ScriptsPropertiesProvider;
import com.empulse.wiltmann.service.JsonFormatterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URI;
import java.util.Optional;

/**
 * RestController class for all webservice endpoints to be used
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-08
 */
@RestController
// @RequestMapping(value = "CF20", consumes = "application/json", produces = "application/json")
public class DataRestController {

    private static final Logger log = LoggerFactory.getLogger(DataRestController.class);

    private final DataService dataService;
    private final FileService fileService;
    private final ScriptsPropertiesProvider scriptsPropertiesProvider;
    private final JsonFormatterService jsonFormatterService;

    @RequestMapping(value = "/Hello")
    public String hello(){
        return "Hello from gitlab to AWS ECS! from team";
    }
    /**
     * @param dataService the service class handling the data methods used
     * @param fileService the service class handling the file methods used
     * @param scriptsPropertiesProvider the service class delivering the properties file that handles R-scripts
     * @param jsonFormatterService
     */
    @Autowired
    public DataRestController(DataService dataService, FileService fileService, ScriptsPropertiesProvider scriptsPropertiesProvider, JsonFormatterService jsonFormatterService) {
        this.dataService = dataService;
        this.fileService = fileService;
        this.scriptsPropertiesProvider = scriptsPropertiesProvider;
        this.jsonFormatterService = jsonFormatterService;
        System.out.println("Inside controller class constructor");
    }

    /**
     * consumes Json-structure to be processed by rWrapper
     * and responses with the result of that script
     *
     * @param inputJson the input Json-structure for the r-script
     * @param scriptName the exact name of the R-script to be used
     * @return output Json-structure from the r-script
     * @TODO the response is still in a pretty raw state
     */
    private ResponseEntity processRScript(String inputJson, String scriptName) {
        System.out.println("inside processRScript and script name is : "+scriptName);
        if (inputJson.isBlank()) {
            throw new EmptyInputJsonException("Input message is empty.");
        }
        if (scriptName.isBlank()) {
            throw new NoScriptNamePassedException("There is no script-name requested.");
        }
        if (!fileService.isScriptExistent(scriptName)) {
            throw new NoSuchScriptException("There is no script with this name found.");
        }
//        System.out.println("input JSON first values  : "+ inputJson);
        Optional<File> fileWritten = dataService.saveJsonInputToFile(jsonFormatterService.minify(inputJson));
        log.debug("json file: {}", fileWritten.map(File::getAbsolutePath));
        System.out.println("in process script ");
        return fileWritten.map(file -> dataService.callrwrapper(scriptName, file).map(ResponseEntity::ok).orElseThrow(()-> new EmptyOutputJsonException("There is no output from R-script."))).orElseThrow(() -> new EmptyOutputJsonException("Error while handling input-json."));
    }

    /**
     * @param inputJson the data json-structure passed into the r-scripts
     * @param request the full path of the endpoint which is equal to the path to the r-script to be used
     * @return the json-structured from the r-script as result of the request
     */
    @PostMapping(value = "/**")
    public ResponseEntity genericEndpoint(@RequestBody String inputJson, HttpServletRequest request) {
        System.out.println("inside postmapping **");
        String fullPath = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
        if(fullPath.charAt(fullPath.length()-1) == '/'){
            fullPath = fullPath.substring(6, fullPath.length() -1);
            System.out.println("full path in if     ="+fullPath);
        }
        else {
            System.out.println("full path in else     ="+fullPath);
            fullPath = fullPath.substring(6);
        }
        String scriptName = scriptsPropertiesProvider.getValue(fullPath.replace("/", "."));
        System.out.println("return from scriptsProprtyiesProvider and script name is  : " +scriptName);
        return processRScript(inputJson, scriptName);
    }

    /**
     * consumes Json-structure and saves to file
     *
     * @param inputJson the input Json-structure
     * @return 201 CREATED
     */
    @PostMapping(value = "/input")
    public ResponseEntity consumeInputJson(@RequestBody String inputJson) {
        System.out.println("inside postmapping input");
        if (inputJson.isBlank()) {
            throw new EmptyInputJsonException("Input is empty.");
        }
        Optional<File> fileWritten = dataService.saveJsonInputToFile(inputJson);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .buildAndExpand(fileWritten)
                .toUri();
        return ResponseEntity.created(location).build();
    }

    /**
     * outputs the latest saved result-file as Json-structure
     *
     * @return the latest saved Json file as a String
     */
    @GetMapping("/output/latest")
    public ResponseEntity outputLatestJson() {
        System.out.println("inside postmapping output");
        Optional<String> latestJson = dataService.getLatestSavedJson();
        if(latestJson.isEmpty()){
            return (ResponseEntity) ResponseEntity.badRequest();
        }
        return ResponseEntity.ok(latestJson);
    }
}
