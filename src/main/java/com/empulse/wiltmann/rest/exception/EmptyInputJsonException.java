package com.empulse.wiltmann.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * exception thrown when InputJson is empty
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-18
 */
@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class EmptyInputJsonException extends RuntimeException {
    private static final long serialVersionUID = 5891481454357155019L;

    /**
     * @param message custom exception message
     */
    public EmptyInputJsonException (String message) {
        super(message);
    }
}
