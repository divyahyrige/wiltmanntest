package com.empulse.wiltmann.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * exception thrown when InputJson is empty
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-18
 */
@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class NoSuchScriptException extends RuntimeException {
    private static final long serialVersionUID = -3140741722171405079L;

    /**
     * @param message custom exception message
     */
    public NoSuchScriptException (String message) {
        super(message);
    }
}