package com.empulse.wiltmann.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * TODO: Add class description
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-06-18
 */
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class NoScriptNamePassedException extends RuntimeException {
    private static final long serialVersionUID = -4839764172273587010L;

    /**
     * @param message custom exception message
     */
    public NoScriptNamePassedException(String message){
        super(message);
    }
}
