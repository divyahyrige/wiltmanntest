package com.empulse.wiltmann.rest.exception;

/**
 * TODO: Add class description
 *
 * @author Tycho Schottelius, empulse GmbH, 2020-07-03
 */
public class EmptyOutputJsonException extends RuntimeException {

    private static final long serialVersionUID = 7299392950874925764L;

    /**
     * @param message custom exception message
     */
    public EmptyOutputJsonException(String message){
        super(message);
    }

}
