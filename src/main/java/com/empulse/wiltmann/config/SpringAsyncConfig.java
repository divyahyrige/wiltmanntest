package com.empulse.wiltmann.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-06-23
 */
@Configuration
@EnableAsync
public class SpringAsyncConfig implements AsyncConfigurer {

    @Value("${thread.pool.core.size}")
    private int threadPoolCoreSize;

    /**
     * This Bean defines a {@link Executor} to be used for the handling R related tasks.
     *
     * @return Executor     to be used for handling R related Tasks
     */
    @Bean(name = "rThreadPoolTaskExecutor")
    public Executor rThreadPoolTaskExecutor() {

        ThreadPoolTaskExecutor exec = new ThreadPoolTaskExecutor();
        exec.setCorePoolSize(threadPoolCoreSize);
        //exec.setMaxPoolSize(400);
        exec.setThreadNamePrefix("R - ");
        exec.initialize();
        System.out.println("Inside asych config class");
        return exec;
    }
}
