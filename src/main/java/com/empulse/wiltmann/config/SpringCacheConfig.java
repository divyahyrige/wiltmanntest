package com.empulse.wiltmann.config;

import com.empulse.wiltmann.statics.ApplicationStatics;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Julian Kupsch, empulse GmbH, 2020-07-07
 */
@Configuration
@EnableCaching
public class SpringCacheConfig  {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache(ApplicationStatics.R_SCRIPT_CACHE_NAME, new ConcurrentHashMap<>(), false)
        ));
        System.out.println("Inside cache config class");

        return cacheManager;
    }
}
