package com.empulse.wiltmann;

import com.empulse.wiltmann.config.SpringAsyncConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * entry point for SpringBoot WebserviceApplication
 */
@SpringBootApplication
@ComponentScan()
public class WebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebserviceApplication.class, args);
	}

}
