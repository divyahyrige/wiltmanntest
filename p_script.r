options(warn=2) # cause the build to fail if any packages fail to install

install.packages(c('geofacet','sweep',"caret", "zoo", "padr", "dplyr","xgboost","RcppRoll","data.table","tidyverse","jsonlite","forecast","Runiversal",'purrr','readr','tstools','factoextra','timetk','tidyquant'))
