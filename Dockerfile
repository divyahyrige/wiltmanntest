FROM divyasgowda/wiltmann:final

#Expose http port
EXPOSE 8080

#The application's jar file
ARG JAR_File=target/wiltmann-0.1.0-SNAPSHOT.jar

#Add the rsript file to container
ADD /r_scripts  /samplep/r_scripts
ADD /application-local.properties /samplep 
ADD /target/wiltmann-0.1.0-SNAPSHOT.jar /samplep
 

#Entrypoint
ENTRYPOINT ["java","-Dspring.config.location=/samplep/application-local.properties" ,"-Dscripts.config.location=/samplep/r_scripts/script.properties", "-jar","samplep/wiltmann-0.1.0-SNAPSHOT.jar"]
